CC=gcc
OP=-O3 -o
ZL=third-party/lib

COMP=source/compress.c header/compress.h
DECP=source/decompress.c header/decompress.h
OBJC=object/comp.o object/decomp.o
ZLIB=$(ZL)/libz.so $(ZL)/libz.so.1 $(ZL)/libz.so.1.2.11 $(ZL)/crc32.h $(ZL)/inffast.h $(ZL)/inftrees.h $(ZL)/zlib.h $(ZL)/deflate.h $(ZL)/inffixed.h $(ZL)/trees.h $(ZL)/zutil.h $(ZL)/gzguts.h $(ZL)/inflate.h $(ZL)/zconf.h

z : $(OBJC) $(ZLIB)
	$(CC) $(OP) binary/z -lz -I$(ZL)/ source/main.c $(OBJC)

#SRC = $(filter-out source/main.c, $(wildcard source/*.c)) $(filter-out header/zerr.h, $(wildcard header/*.h))

object/comp.o : $(COMP)
	$(CC) -c $(OP) object/comp.o source/compress.c

object/decomp.o : $(DECP)
	$(CC) -c $(OP) object/decomp.o source/decompress.c

clean :
	yes | rm -vf binary/*
	yes | rm -vf object/*
