# SIMPLE EXAMPLE OF zlib
this program is a simple example of [zlib How to Use](https://zlib.net/zlib_how.html).
## Requirements
* GCC 4 and above.
* zlib 1.2.11
* GNU Make
## Build
+ rebuild zlib in `third-party/zlib/*`
+ copy every `*.so*` and `*.h` to `third-party/lib`
+ run `make`
## Usage
for test i put a huge text (The GNU C Programming book) in `test` directory. use that for testing: </br>
`binary/z < test/huge.txt > test/huge.txt.zlib` </br>
and for decompressing: </br>
`binary/z -d < test/huge.txt.zlib > test/decompressed.txt`

