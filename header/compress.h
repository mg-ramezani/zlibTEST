#ifndef HEADER_COMPRESS_H
#define HEADER_COMPRESS_H

#include "include.h"

/*
 * Compress From File Source To File
 * Destenation Until EOF On Source,
 * comp() returns Z_OK On Success.
 */
int comp(FILE *, FILE *, int);

#endif
