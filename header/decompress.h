#ifndef HEADER_DECOMPRESS_H
#define HEADER_DECOMPRESS_H

#include "include.h"

/*
 * Decompresse From File Source to
 * File Destenation Until Stream Ends
 * Or EOF. decomp() returns Z_OK On
 * Success.
 */
int decomp(FILE *, FILE *);


#endif
