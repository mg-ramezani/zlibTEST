#ifndef HEADER_INCLUDE_H
#define HEADER_INCLUDE_H

#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "../third-party/zlib-1.2.11/zlib.h"

/*
 * This Is An Ugly Hack Required When
 * Program Compiled On Unusual OS.
 * Actualy We Aviod Corruption Of The-
 * Input And Output Data On Windows/MS-DOS
 * Systems.
 * For More Information Read :
 * https://zlib.net/zlib_how.html
 */
#if defined (MSDOS) || defined (OS2) || defined(WIN32) \
    || defined (__CYGWIN__)
#    include <fcntl.h>
#    include <io.h>
#    define SET_BINARY_MODE(file) setmode(fileno(file), O_BINARY)
#else
#    define SET_BINARY_MODE(file)
#endif

/*
 * CHUNK Is Simply The Buffer Size For 
 * Feeding Data To And Pulling Data
 * From The zlib Routines.
 */
#ifndef CHNUK
#    define CHUNK 16384
#endif

#endif
