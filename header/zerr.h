#ifndef HEADER_ZERR_H
#define HEADER_ZERR_H

/* Report A zlib Or I/O Error */
void zerr(int ret){
    fprintf(stderr, "ZPIPE: ");
    switch (ret){
        case Z_ERRNO:
            if (ferror(stdin))
                fprintf (stderr, "[ERROR READING stdin]\n");
            if (ferror(stdout))
                fprintf (stderr, "[ERROR WRITING stdout]\n");
            break;
        case Z_STREAM_ERROR:
            fprintf (stderr, "[INVALID COMPRESSION LEVEL]\n");
            fprintf (stderr, "\t-[USE -1 TO 9]\n");
            break;
        case Z_DATA_ERROR:
            fprintf (stderr, "[INVALID OR INCOMPLETE DEFLATE DATA]\n");
            break;
        case Z_MEM_ERROR:
            fprintf (stderr, "[OUT OF MEMORY]\n");
            break;
        case Z_VERSION_ERROR:
            fprintf (stderr, "[zlib VERSION MISMATH]\n");
            break;
    }
}

#endif
