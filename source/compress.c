#include "../header/compress.h"

#define ZZ_ALLOCATE() stream.zalloc = Z_NULL;\
                      stream.zfree  = Z_NULL;\
                      stream.opaque = Z_NULL;

/*
 * I Use :
 *    https://zlib.net/zlib_how.html
 * For Writing Those Function.
 */

int comp(FILE *source, FILE *dest, int level){
    int ret, flush;
    unsigned have;
    z_stream stream;
    unsigned char in [CHUNK];
    unsigned char out[CHUNK];

    /* Allocate deflate() State */
    ZZ_ALLOCATE()
    ret = deflateInit(&stream, level);
    if (ret != Z_OK)
        return ret;

    /* Compress Point */
    do {
        stream.avail_in = fread(in, 1, CHUNK, source);
        if (ferror(source)){
            deflateEnd(&stream);
            return Z_ERRNO;
        }
        flush = feof(source) ? Z_FINISH : Z_NO_FLUSH;
        stream.next_in = in;
        do {
            stream.avail_out = CHUNK;
            stream.next_out  = out;
            ret = deflate(&stream, flush);
            assert(ret !=  Z_STREAM_ERROR);
            have = CHUNK - stream.avail_out;

            if (fwrite(out, 1, have, dest) != have || ferror(dest)){
                deflateEnd(&stream);
                return Z_ERRNO;
            }
        } while (stream.avail_out == 0);
        assert(stream.avail_in == 0);
    } while (flush != Z_FINISH);
    assert(ret == Z_STREAM_END);

    /* Clean Up And Return */
    deflateEnd(&stream);
    return Z_OK;
}
