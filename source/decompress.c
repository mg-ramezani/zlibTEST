#include "../header/decompress.h"

#define ZZ_ALLOCATE() stream.zalloc = Z_NULL;\
                      stream.zfree  = Z_NULL;\
                      stream.opaque = Z_NULL;


int decomp(FILE *source, FILE *dest){
    int ret;
    unsigned have;
    z_stream stream;
    unsigned char in [CHUNK];
    unsigned char out[CHUNK];

    ZZ_ALLOCATE()
    stream.avail_in = 0;
    stream.next_in  = Z_NULL;
    ret = inflateInit(&stream);
    if (ret != Z_OK)
        return ret;

    /* Decompress Point */
    do {
        stream.avail_in = fread(in, 1, CHUNK, source);
        if (ferror(source)){
            inflateEnd(&stream);
            return Z_ERRNO;
        }
        if (stream.avail_in == 0)
            break;
        stream.next_in = in;

        do {
            stream.avail_out = CHUNK;
            stream.next_out  = out;

            ret = inflate(&stream, Z_NO_FLUSH);
            assert(ret != Z_STREAM_ERROR);
            switch (ret) {
                case Z_NEED_DICT :
                    ret = Z_DATA_ERROR;
                case Z_DATA_ERROR:
                case Z_MEM_ERROR :
                    inflateEnd(&stream);
                    return ret;
            }

            have = CHUNK - stream.avail_out;
            if (fwrite(out, 1, have, dest) != have || ferror(dest)){
                inflateEnd(&stream);
                return Z_ERRNO;
            }
        } while (stream.avail_out == 0);
    } while (ret != Z_STREAM_END);
    inflateEnd(&stream);

    return ret;
}

