#include "../header/decompress.h"
#include "../header/compress.h"
#include "../header/zerr.h"

int main (int argc, char **argv){
    int ret;

    SET_BINARY_MODE(stdin);
    SET_BINARY_MODE(stdout);

    if (argc == 1){
        ret = comp(stdin, stdout, 9);
        if (ret != Z_OK)
            zerr(ret);
        return ret;
    } else if (argc == 2 && strcmp(argv[1], "-d") == 0){
        ret = decomp(stdin, stdout);
        if (ret != Z_OK)
            zerr(ret);
        return ret;
    } else {
        puts("ZPIPE USAGE: ZPIPE [-d] < source > dest");
        return 1;
    }
    return 0;
}
